<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress_1' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'qwerty' );

/** Database hostname */
define( 'DB_HOST', '192.168.100.2:3310' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'V$yTj)zc1RR3t?#ps&WkEaJtO/5|h>I6G2FP/{vAG@6K4hb2oyYtM{<at,gBOROA' );
define( 'SECURE_AUTH_KEY',  '<CURo[;tPV(;dE` j?H`.R?5Hb}S05zOm-q|eB+tlNwfI+N!{jXLx5YY4F`sZc$S' );
define( 'LOGGED_IN_KEY',    '{+[:[aQ5P<:ww=-Z1@z[;q*Qs&<$d|-{j6qPF)=E4wxoDYeWNfyq Jsb}LKh/);k' );
define( 'NONCE_KEY',        'vwPj52#$%B4(dj*9uU~mg2KQE,_%XsT8ah)!I)35=*qiAA%A}h=,JHSMP+n/c#<H' );
define( 'AUTH_SALT',        'X~}}:hB.9cYSmY?Zo;70Aa,^EZ_T&GJ0GQ1T1,U98M_^UW(rS%_;owli3},$ M<u' );
define( 'SECURE_AUTH_SALT', 'NyV[VcXD@]kP$Tll31M0-lQXAwQS]Us<bz_gUm:41o^6uGolrs?J*udvXLM&P3s]' );
define( 'LOGGED_IN_SALT',   '$U`4rMU!E4uL=LBhsRW/-X<p?x=6^7R;Jynw/1Qx[5|-pFWGT6KL+! mfF#}I7vZ' );
define( 'NONCE_SALT',       '-1U&peEygXtZ#z3}#5!3`Fn,@}9_lFz_*Ht0KrP@Q2(;9dO?Y$6hOCm%dqTMzc _' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
